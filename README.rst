Created by `Md. Abdul Munim Dibosh` using *reStructuredText*

======================================================================
 CASGiftbox - An app developed for annual campaign of CompAndSave.com
======================================================================

Introduction
============
The android version of this app was released but the iOS version could not be released due to some problems from Apple authority. Thought sharing the code could be helpful for people willing to learn Objective-C.


A very simple user interface. Can be used to learn about:

1. UITableView
2. Custom scrolling view
3. Timers
4. NSDictionary
5. .plist files and their importance
6. UILocalNotification : basics and simple customization
7. NSUserDefaults
8. View transitions using simple animations

An interesting UI pattern introduced:

Once you have a list of items (a list pretty long that it goes beyong screen height) and you have a translucent-floating view hovering over the list,you have to put some mecanism so that the view doesn't block user interactions with the list-items,ever! Created a custom view that will hover over the listview and will keep changing it's position once the listview is scrolled.

.. image:: https://bitbucket.org/coderguy194/casgiftbox-simple-campaign-app-for-compandsave.com/raw/master/casgiftbox.gif


Open Source
===========
This is an Open Source project. If you could add some other interesting stuffs don't forget to shoot a `mail`_. I would love to know.

.. GENERAL LINKS

.. _`mail`: abdulmunim.buet@gmail.com