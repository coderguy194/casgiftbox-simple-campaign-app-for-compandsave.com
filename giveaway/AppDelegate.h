//
//  AppDelegate.h
//  giveaway
//
//  Created by Md. Abdul Munim Dibosh on 11/16/13.
//  Copyright (c) 2013 Md. Abdul Munim Dibosh. All rights reserved.
//

#import <UIKit/UIKit.h>
#define CHOOSE_ITEMS_SCREEN @"Choose_Items_Screen"
#define MAIN_SCREEN @"Main_Screen"


@interface AppDelegate : UIResponder <UIApplicationDelegate>

//those which are valid from today to endday of giveaway
@property NSMutableArray *giftChoiceObjects;
//the whole list of gifts
@property NSMutableArray *giftObjects;
@property NSMutableArray *preSelectedChoiceList;
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UIStoryboard *storyboard;


@end
