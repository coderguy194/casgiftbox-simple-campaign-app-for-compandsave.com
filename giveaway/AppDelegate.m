//
//  AppDelegate.m
//  giveaway
//
//  Created by Md. Abdul Munim Dibosh on 11/16/13.
//  Copyright (c) 2013 Md. Abdul Munim Dibosh. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewHelper.h"

@implementation AppDelegate
@synthesize storyboard,giftChoiceObjects,giftObjects,preSelectedChoiceList;

-(void)listAllFonts
{
    for (NSString* family in [UIFont familyNames])
    {
        for (NSString* name in [UIFont fontNamesForFamilyName: family])
        {
            NSLog(@"  %@", name);
        }
    }
}
-(void)createMildNotificationForEachGift
{
    for(GiftChoice *gift in giftChoiceObjects)
    {
        int alarmDay = gift.alarmDay;
        //create alarm for those days
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm a"];
        NSLog(@"Alarm day=%d",alarmDay);
        NSDate *firstAlarm = [dateFormatter dateFromString:[NSString stringWithFormat:@"2013-12-%d 12:00 AM",alarmDay]];
        NSString *alertBody1 = [NSString stringWithFormat:@"Win amazing gifts by buying ink! Todays gift : %@.Check the app for details!",gift.title];
        [ViewHelper scheduleNotificationForDate:firstAlarm AlertBody:alertBody1 ActionButtonTitle:@"Open app" NotificationID:@"Mild" WithSeverity:NO];
    }
}
-(void)chooseProperViewController
{
    //decide which screen to show if this is first launch
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *key = @"showChooserAtStartup";//key for if chosen proper items
    // Get the value
    NSString *showChooserAtStartup = [defaults stringForKey:key];
    
    //whatever the first controller we have to show the splash
    UIViewController *initialViewController;
    //item chooser should show up (nil indicates first launch)
    if(showChooserAtStartup == nil && [ViewHelper isTodayAfterCampaign]== NO)
    {
        initialViewController = [storyboard instantiateViewControllerWithIdentifier:CHOOSE_ITEMS_SCREEN];
        //for the first launch,show add mild notificaton for each item
        [self createMildNotificationForEachGift];
    }
    else{
        //not first launch at all
        initialViewController = [storyboard instantiateViewControllerWithIdentifier:MAIN_SCREEN];
    }
    
    // create the Navigation Controller instance:
//    UINavigationController *newnav = [[UINavigationController alloc] initWithRootViewController:initialViewController];
//    
//    // set the navController property:
//    [self setNavController:newnav];
//    self.window.rootViewController = navController;
//    [navController setNavigationBarHidden:YES];
    
    [ViewHelper showSplash:initialViewController.view];
    
    self.window.rootViewController = initialViewController;
    [self.window makeKeyAndVisible];
}
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
//    [self listAllFonts];
    storyboard= [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil];
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    
    application.applicationIconBadgeNumber = [ViewHelper getGiveAwayEndsDaysLeft]/86400;
    giftChoiceObjects = [NSMutableArray arrayWithArray:[ViewHelper getGiftObjectsArray:YES]];
    giftObjects = [NSMutableArray arrayWithArray:[ViewHelper getGiftObjectsArray:NO]];
    preSelectedChoiceList = [NSMutableArray arrayWithArray:[ViewHelper getSelectedChoiceList]];
    NSLog(@"delegate:choices:%d",[giftChoiceObjects count]);
    NSLog(@"delegate:gifts:%d",[giftObjects count]);
    [self chooseProperViewController];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
