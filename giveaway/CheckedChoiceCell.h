//
//  CheckedChoiceCell.h
//  giveaway
//
//  Created by Md. Abdul Munim Dibosh on 11/17/13.
//  Copyright (c) 2013 Md. Abdul Munim Dibosh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckedChoiceCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *chckBox;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property BOOL isSelected;

-(void)setTitle:(NSString*)title;
@end
