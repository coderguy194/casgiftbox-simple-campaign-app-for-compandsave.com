//
//  CheckedChoiceCell.m
//  giveaway
//
//  Created by Md. Abdul Munim Dibosh on 11/17/13.
//  Copyright (c) 2013 Md. Abdul Munim Dibosh. All rights reserved.
//

#import "CheckedChoiceCell.h"
@implementation CheckedChoiceCell
@synthesize lblTitle,chckBox,isSelected;
//- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
//{
//    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
//    if (self) {
//        // Initialization code
//    }
//    return self;
//}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    isSelected = selected;
    
    if(isSelected)
    {
        [chckBox setImage:[UIImage imageNamed:@"checkbox_full.png"]];
    }
    else{
        [chckBox setImage:[UIImage imageNamed:@"checkbox_empty.png"]];
    }
}

-(void)setTitle:(NSString*)title
{
    [lblTitle setText:title];
}

@end
