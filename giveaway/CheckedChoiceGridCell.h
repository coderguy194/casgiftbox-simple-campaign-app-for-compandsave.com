//
//  CheckedChoiceGridCell.h
//  giveaway
//
//  Created by Md. Abdul Munim Dibosh on 11/17/13.
//  Copyright (c) 2013 Md. Abdul Munim Dibosh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckedChoiceGridCell : UICollectionViewCell
@property(nonatomic, weak) IBOutlet UILabel *lblTitle;
@property(nonatomic, weak) IBOutlet UILabel *lblDate;
@property(nonatomic, weak) IBOutlet UIImageView *icon;
@property(nonatomic, weak) IBOutlet UIImageView *chckBx;

@end
