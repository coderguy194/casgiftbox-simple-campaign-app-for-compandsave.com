//
//  Definitions.h
//  giveaway
//
//  Created by Md. Abdul Munim Dibosh on 11/22/13.
//  Copyright (c) 2013 Md. Abdul Munim Dibosh. All rights reserved.
//

#ifndef giveaway_Definitions_h
#define giveaway_Definitions_h
#define FONT_MUSEO_NORMAL @"MuseoSlab-500"
#define FONT_MUSEO_ITALIC @"MuseoSlab-500Italic"
#define FONT_CENTURYGOTHIC_ITALIC @"CenturyGothic-Italic"
#define FONT_CENTURYGOTHIC_BOLD @"CenturyGothic-Bold"
#define FONT_CENTURYGOTHIC @"CenturyGothic"

#define FNT_MUSEO_F(SIZE) [UIFont fontWithName:FONT_MUSEO_NORMAL size:SIZE]
#define FNT_CGI_F(SIZE) [UIFont fontWithName:FONT_CENTURYGOTHIC_ITALIC size:SIZE]
#define FNT_CGB_F(SIZE) [UIFont fontWithName:FONT_CENTURYGOTHIC_BOLD size:SIZE]
#define FNT_CG_F(SIZE) [UIFont fontWithName:FONT_CENTURYGOTHIC size:SIZE]



#endif
