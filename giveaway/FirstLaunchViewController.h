//
//  FirstLaunchViewController.h
//  giveaway
//
//  Created by Md. Abdul Munim Dibosh on 11/17/13.
//  Copyright (c) 2013 Md. Abdul Munim Dibosh. All rights reserved.
//

#import <UIKit/UIKit.h>
#define SAVE_BUTTON_TAG 345
#define LATER_BUTTON_TAG 346

@interface FirstLaunchViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate>
{
//    NSMutableArray *giftChoiceObjects;
//    NSMutableArray *selectedItems;
    BOOL canAnimate ;//controls simultaneous animations
    CGPoint locationBottom;//the location where the overlay should animate from top
    CGPoint locationTop;//the location where the overlay should animate from bottom
    
}

@property NSInteger SECTIONS;

@property(nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property(nonatomic, weak) IBOutlet UIView *bottomOverlay;
@property(nonatomic, weak) IBOutlet UIButton *btnSave;
@property(nonatomic, weak) IBOutlet UIButton *btnLater;
- (IBAction)btnPressed:(id)sender;

@property(nonatomic, weak) IBOutlet UILabel *titleLabel;



@end
