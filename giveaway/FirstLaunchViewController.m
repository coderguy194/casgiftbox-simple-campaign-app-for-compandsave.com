//
//  FirstLaunchViewController.m
//  giveaway
//
//  Created by Md. Abdul Munim Dibosh on 11/17/13.
//  Copyright (c) 2013 Md. Abdul Munim Dibosh. All rights reserved.
//

#import "FirstLaunchViewController.h"
#import "CheckedChoiceGridCell.h"
#import "GiftChoice.h"
#import <QuartzCore/QuartzCore.h>
#import "Definitions.h"
#import "ViewHelper.h"
#import "AppDelegate.h"
#import "MainScreen.h"
#define ROW_PER_SEC 12

@interface FirstLaunchViewController ()

@end

@implementation FirstLaunchViewController
@synthesize SECTIONS;
static NSString *cellIdentifier=@"CheckedChoiceGridCell";
#pragma mark - selection methods
//denotes if current choice is in previously selected list
-(BOOL)isItemSelectedBefore:(NSString*)ITM_ID
{
    for (NSString *ID in [ViewHelper getSharedApplicationInstance].preSelectedChoiceList) {
        if([ID isEqualToString:ITM_ID])
        {
            return YES;
        }
    }
    return NO;
}
//if user has deselected it,we must remove it from previous data
-(void)deleteSelectionFromPrevious:(NSString*)ID
{
    [[ViewHelper getSharedApplicationInstance].preSelectedChoiceList removeObject:ID];
}
#pragma mark - UICollectionView Datasource

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    
    return [[ViewHelper getSharedApplicationInstance].giftChoiceObjects count];
}
// 2
- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    
    return SECTIONS;
}
// 3
- (CheckedChoiceGridCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CheckedChoiceGridCell *cell = [cv dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    int row = indexPath.row;
    
    if (cell == nil) {
        
        NSArray* views = [[NSBundle mainBundle] loadNibNamed:@"CheckedChoiceGridCell" owner:nil options:nil];
        
        for (UIView *view in views) {
            if([view isKindOfClass:[UICollectionViewCell class]])
            {
                cell = (CheckedChoiceGridCell*)view;
            }
        }
    }
    
    
    GiftChoice *choice = [[ViewHelper getSharedApplicationInstance].giftChoiceObjects objectAtIndex:row];
//    GiftChoice *choice = [giftChoiceObjects objectAtIndex:section* ROW_PER_SEC +row];
    
    [cell.lblTitle setText:[choice.title uppercaseString]];
    [cell.lblDate setText:[choice.date uppercaseString]];
    [cell.icon setImage:choice.icon];
    //set fonts
    [cell.lblTitle setFont:FNT_CGB_F(11)];
    [cell.lblDate setFont:FNT_CG_F(13)];
    //add some more depth
    [ViewHelper applyShadowToView:cell.lblDate WithSize:CGSizeMake(0.3, 0.3)];
    [ViewHelper applyShadowToView:cell.lblTitle WithSize:CGSizeMake(0.3, 0.3)];
     
    [cell.lblTitle setTextColor:[UIColor colorWithRed:160/255.0 green:29/255.0 blue:40/255.0 alpha:1]];
    
    cell.backgroundColor = [UIColor colorWithRed:252/255.0 green:247/255.0 blue:231/255.0 alpha:1];
    
    [cell.chckBx setImage:[UIImage imageNamed:@"checkbox_empty.png"]];
    
    if([self isItemSelectedBefore:choice.ID])
    {
        //this choice was selected before
        choice.isSelected = YES;
    }
    
    if(choice.isSelected)
    {
        [cell.chckBx setImage:[UIImage imageNamed:@"checkbox_full.png"]];
    }
    
    //add some shadow
    cell.layer.masksToBounds = NO;    
    cell.layer.shadowOpacity = 1.0;
    cell.layer.shadowRadius = 0.0;
    cell.layer.shadowColor = [UIColor blackColor].CGColor;
    cell.layer.shadowOffset = CGSizeMake(1,1);
    cell.layer.shadowRadius = 1.0f;
    
    
    return cell;
}




#pragma mark - Helper Methods
- (CGFloat)verticalOffsetForTop :(UIScrollView*)sv{
    CGFloat topInset = sv.contentInset.top;
    return -topInset;
}

- (CGFloat)verticalOffsetForBottom :(UIScrollView*)sv{
    CGFloat scrollViewHeight = sv.bounds.size.height;
    CGFloat scrollContentSizeHeight = sv.contentSize.height;
    CGFloat bottomInset = sv.contentInset.bottom;
    CGFloat scrollViewBottomOffset = scrollContentSizeHeight + bottomInset - scrollViewHeight;
    return scrollViewBottomOffset;
}
- (BOOL)isAtTop:(UIScrollView*)sv{
    
    return (sv.contentOffset.y <= [self verticalOffsetForTop:sv]);
}

- (BOOL)isAtBottom:(UIScrollView*)sv{
    
    return (sv.contentOffset.y >= [self verticalOffsetForBottom:sv]);
}


#pragma mark- some helpers
-(NSArray*)selectedGiftItems
{
    NSMutableArray *array = [NSMutableArray array];
    for (GiftChoice *choice in [ViewHelper getSharedApplicationInstance].giftChoiceObjects) {
        if(choice.isSelected == YES)
        {
            [array addObject:choice.ID];
        }
    }
    return array;
}
#pragma mark -UIScrollViewDelegate
-(void)scrollViewDidScroll: (UIScrollView*)scrollView
{
    
    canAnimate = YES;
    float y = locationBottom.y-scrollView.contentOffset.y*1.8;
    
    if(y < (80+ self.bottomOverlay.frame.size.height/2))
    {
        y = 80+ self.bottomOverlay.frame.size.height/2;
    }
    
    if(y > locationBottom.y)
    {
        y = locationBottom.y;
    }
    
    self.bottomOverlay.center = CGPointMake(self.bottomOverlay.center.x,y);
    
    if ([self isAtTop:scrollView])
    {
        if(canAnimate)
        {
            canAnimate = NO;
//            [self goDown];
        }
            
    }
    else if ([self isAtBottom:scrollView])
    {
        if(canAnimate)
        {
            canAnimate = NO;
//            [self goUp];
        }
    }
}


// 4
/*- (UICollectionReusableView *)collectionView:
 (UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
 {
 return [[UICollectionReusableView alloc] init];
 }*/


#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    GiftChoice *choice = [[ViewHelper getSharedApplicationInstance].giftChoiceObjects objectAtIndex:indexPath.section * ROW_PER_SEC +indexPath.row];
    choice.isSelected = !choice.isSelected;
    //user deselected a previously selected one
    if(!choice.isSelected && [self isItemSelectedBefore:choice.ID])
    {
        [self deleteSelectionFromPrevious:choice.ID];
        
        //cancel the notifications for these days.
        [ViewHelper cancelLocalNotification:[NSString stringWithFormat:@"%@One",choice.title]];
        [ViewHelper cancelLocalNotification:[NSString stringWithFormat:@"%@Two",choice.title]];
    }
        
    [self.collectionView reloadData];
}

#pragma mark – UICollectionViewDelegateFlowLayout

float item_spacing = 1;
float section_spacing = 10;
int width  = 92;
int height = 130;

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGSize retval = CGSizeMake(width, height);
    return retval;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(10,10,10,10);
}

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return section_spacing;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return item_spacing;
}



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
//find the giftchoice object with specified id
-(GiftChoice*)getGiftWithID:(NSString*)ID
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ID == %@",ID];
    //find out the gift using its id from the given list of can-be-chosen objects
    NSArray *filteredArray = [((AppDelegate*)[ViewHelper getSharedApplicationInstance]).giftChoiceObjects filteredArrayUsingPredicate:predicate];
    GiftChoice* firstFoundObject = nil;
    if ([filteredArray count] > 0) {
        firstFoundObject = [filteredArray objectAtIndex:0];
        NSLog(@"%@,%d",firstFoundObject.title,firstFoundObject.alarmDay);
    }
    
    return firstFoundObject;

}
- (IBAction)btnPressed:(id)sender
{
    int tag = ((UIButton*)sender).tag;
    switch (tag) {
        case SAVE_BUTTON_TAG:
        {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSString *key = @"showChooserAtStartup";
            
            NSArray *items = [self selectedGiftItems];
            if([items count] > 0)
            {
                
                [defaults setObject:@"NO" forKey:key];//just put something so that it is not nil anymore
                [defaults synchronize]; // this method is optional
                
                [ViewHelper saveSelectedChoiceList:items];
                
                for(NSString *item in items)
                {
                    GiftChoice *gift = [self getGiftWithID:item];
                    int alarmDay = gift.alarmDay;
                    //create alarm for those days
                    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                    [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm a"];
                    NSLog(@"Alarm day=%d",alarmDay);
                    NSDate *firstAlarm = [dateFormatter dateFromString:[NSString stringWithFormat:@"2013-12-%d 9:00 AM",alarmDay]];
                    NSDate *scndAlarm = [dateFormatter dateFromString:[NSString stringWithFormat:@"2013-12-%d 6:30 PM",alarmDay]];
                    NSString *alertBody1 = [NSString stringWithFormat:@"Win a %@ today. Order your ink now and enter the giveaway. Use “XMAS12” at checkout to get 10%% OFF[www.compandsave.com/giveaway]",gift.title];
                    NSString *alertBody2 = [NSString stringWithFormat:@"There is still time to win a %@. Order your ink by 11:59pm (PST) and enter the giveaway. Use “XMAS12” at checkout to get 10%% OFF [www.compandsave.com/giveaway]",gift.title];
                    [ViewHelper scheduleNotificationForDate:firstAlarm AlertBody:alertBody1 ActionButtonTitle:@"Open app" NotificationID:[NSString stringWithFormat:@"%@One",gift.title] WithSeverity:YES];
                    [ViewHelper scheduleNotificationForDate:scndAlarm AlertBody:alertBody2 ActionButtonTitle:@"Open app" NotificationID:[NSString stringWithFormat:@"%@Two",gift.title] WithSeverity:YES];
                }
                
                
                NSMutableDictionary *bundle = [[NSMutableDictionary alloc] init];
                [bundle setValue:@"Thank you for choosing your favorite gift item(s)." forKey:@"title"];
                [bundle setValue:@"You will be reminded to enter the giveaway when the items you select become available. Check the campaign page for the latest news and updates." forKey:@"msg"];
                
                [ViewHelper showViewController:MAIN_SCREEN WithData:bundle];

                
            }
            else{
                UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"No item selected!" message:@"Please select one or more to save as your choice." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alertView show];
            }
        }
        break;
            
        default:
        {
            NSMutableDictionary *bundle = [[NSMutableDictionary alloc] init];
            if([[self selectedGiftItems] count] == 0)
            {
                
                [bundle setValue:@"Oops!You didn't select any item." forKey:@"title"];
                [bundle setValue:@"Select favorite gift item(s) before the giveaway starts!You will get reminders on the date(s) your items(s) will be available to enter the giveaway and win your selected gift(s).Edit choices or go to campaign page to know more." forKey:@"msg"];
            }
            [ViewHelper showViewController:MAIN_SCREEN WithData:bundle];
        }
        break;
    }
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    [self.btnSave setBackgroundImage:[UIImage imageNamed:@"btnSelected_bg.png"] forState:UIControlEventTouchUpInside];
    [self.btnLater setBackgroundImage:[UIImage imageNamed:@"btnSelected_bg.png"] forState:UIControlEventTouchUpInside];
    [self.btnSave.titleLabel setFont:FNT_MUSEO_F(17)];
    [self.btnLater.titleLabel setFont:FNT_MUSEO_F(17)];
    [ViewHelper applyShadowToView:self.btnSave.titleLabel WithSize:CGSizeMake(1, 1)];
    [ViewHelper applyShadowToView:self.btnLater.titleLabel WithSize:CGSizeMake(1, 1)];
    [self.btnSave showsTouchWhenHighlighted];
    [self.btnLater showsTouchWhenHighlighted];
    
    [self.titleLabel setFont:FNT_MUSEO_F(17)];
    [ViewHelper applyShadowToView:self.titleLabel WithSize:CGSizeMake(1, 1)];
    
    
//    self.view.backgroundColor= [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.png"]];
    
    
    locationBottom = self.bottomOverlay.center;
    
    locationTop = CGPointMake(self.bottomOverlay.center.x, self.bottomOverlay.center.y-200);
    
    canAnimate = NO;
    
    //register custom class and nib for each collection view cells
    [self.collectionView registerClass:[CheckedChoiceGridCell class] forCellWithReuseIdentifier:@"CheckedChoiceGridCell"];
    
    UINib *cellNib = [UINib nibWithNibName:@"CheckedChoiceGridCell" bundle:nil];
    [self.collectionView registerNib:cellNib forCellWithReuseIdentifier:@"CheckedChoiceGridCell"];
    
    SECTIONS =1;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
