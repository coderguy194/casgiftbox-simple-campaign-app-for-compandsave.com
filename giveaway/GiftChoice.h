//
//  GiftChoice.h
//  giveaway
//
//  Created by Md. Abdul Munim Dibosh on 11/17/13.
//  Copyright (c) 2013 Md. Abdul Munim Dibosh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GiftChoice : NSObject

@property BOOL isSelected;
@property NSString *date;
@property NSString *title;
@property UIImage *icon;
@property int alarmDay;
//a MD5 hash from title
@property NSString *ID;
-(BOOL)isTodayValidForThisGift;

@end
