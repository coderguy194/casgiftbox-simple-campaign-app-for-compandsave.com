//
//  GiftChoice.m
//  giveaway
//
//  Created by Md. Abdul Munim Dibosh on 11/17/13.
//  Copyright (c) 2013 Md. Abdul Munim Dibosh. All rights reserved.
//

#import "GiftChoice.h"

@implementation GiftChoice
@synthesize icon,date,title,isSelected,ID;
- (NSInteger)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime {
    NSDate *fromDate;
    NSDate *toDate;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    [calendar rangeOfUnit:NSDayCalendarUnit startDate:&fromDate
                 interval:NULL forDate:fromDateTime];
    [calendar rangeOfUnit:NSDayCalendarUnit startDate:&toDate
                 interval:NULL forDate:toDateTime];
    NSDateComponents *differenceComponents = [calendar components:NSDayCalendarUnit
                                                         fromDate:fromDate
                                                           toDate:toDate
                                                          options:0];
    return [differenceComponents day];
}

-(BOOL)isTodayValidForThisGift
{
    NSArray *firstSplit = [self.date componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *datePart = [firstSplit lastObject];
    NSLog(@"GiftChoice::20::%@ Found",datePart);
    
    
    NSDate *today = [NSDate date];
    
    if([datePart isEqualToString:@"21-23"] || [datePart isEqualToString:@"28-30"])
    {
        NSLog(@"Here!");
        //this date has a range of dates
        NSArray *scndSplit = [datePart componentsSeparatedByString:@"-"];
        int firstDate = [[scndSplit objectAtIndex:0] intValue];
        int lastDate =[[scndSplit lastObject] intValue];
        NSDateFormatter* df = [[NSDateFormatter alloc] init];
        //        [df setDateFormat:@"yyyy-MM-dd hh:mm a"];
        [df setDateFormat:@"MM/dd/yyyy"];
        NSDate *firstDay = [df dateFromString:[NSString stringWithFormat:@"12/%d/2013",firstDate] ];
        NSDate *lastDay = [df dateFromString:[NSString stringWithFormat:@"12/%d/2013",lastDate] ];
        
        int diffWithFrstDay =  abs([self daysBetweenDate:today andDate:firstDay]);
        int diffWith2ndDay = abs([self daysBetweenDate:today andDate:lastDay]);
        NSLog(@"diff1=%d,diff2=%d",diffWithFrstDay,diffWith2ndDay);
        if(diffWithFrstDay == 0 || (diffWithFrstDay==1 && diffWith2ndDay==1) || diffWith2ndDay == 0)
        return YES;
    }
    
    
    int day = [datePart intValue];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//        [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm a"];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    NSDate *firstDay = [dateFormatter dateFromString:[NSString stringWithFormat:@"12/%d/2013",day]];
//        NSDate *lastDay = [dateFormatter dateFromString:[NSString stringWithFormat:@"2013-12-%d 12:00 AM",day+1]];
    int diff = [self daysBetweenDate:today andDate:firstDay];
    
    NSLog(@"%d",diff);
    
    return  diff== 0;
   
}

@end
