//
//  ViewController.h
//  giveaway
//
//  Created by Md. Abdul Munim Dibosh on 11/16/13.
//  Copyright (c) 2013 Md. Abdul Munim Dibosh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewHelper.h"
#import "GiftChoice.h"
#define BEFORE_CAMPAIGN_VIEW 4567
#define ON_CAMPAIGN_VIEW 4568
#define AFTER_CAMPAIGN_VIEW 4569

@interface MainScreen : UIViewController
{
    NSString *title;
    NSString *msg;
    BOOL contentPaneFull;
}
@property NSDictionary *bundle;
@property NSURL *urlToGo;
//the hours,days,mins labels to show before campaign
@property NSMutableArray *tmLabels;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIView *contentPane;
@property (weak, nonatomic) IBOutlet UIButton *btnEditGfts;
@property (weak, nonatomic) IBOutlet UIButton *btnGoToPage;

- (IBAction)btnPressed:(id)sender;


@end
