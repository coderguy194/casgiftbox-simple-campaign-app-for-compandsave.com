//
//  ViewController.m
//  giveaway
//
//  Created by Md. Abdul Munim Dibosh on 11/16/13.
//  Copyright (c) 2013 Md. Abdul Munim Dibosh. All rights reserved.
//

#import "MainScreen.h"
#import "CheckedChoiceCell.h"
#import "Definitions.h"
#import "AppDelegate.h"
#define BTN_EDIT 196
#define BTN_NEXT 194
#define BTN_GOTO 195
#define CELL_HEIGHT 64
@interface MainScreen ()

@end

@implementation MainScreen
@synthesize bundle,urlToGo,tmLabels;
//find the giftchoice object with specified id
-(GiftChoice*)getGiftWithID:(NSString*)ID
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ID == %@",ID];
    //find out the gift using its id from the given list of can-be-chosen objects
    NSArray *filteredArray = [((AppDelegate*)[ViewHelper getSharedApplicationInstance]).giftObjects filteredArrayUsingPredicate:predicate];
    GiftChoice* firstFoundObject = nil;
    if ([filteredArray count] > 0) {
        firstFoundObject = [filteredArray objectAtIndex:0];
        NSLog(@"%@,%d",firstFoundObject.title,firstFoundObject.alarmDay);
    }
    
    return firstFoundObject;
    
}
- (IBAction)btnPressed:(id)sender
{
    int tag = ((UIButton*)sender).tag;
    switch (tag) {
        case BTN_EDIT:
            [ViewHelper showViewController:CHOOSE_ITEMS_SCREEN WithData:nil];
            break;
            
        case BTN_GOTO:
            [[UIApplication sharedApplication] openURL:urlToGo];
            break;
    }
    
}
-(void)stylizeAllLabels:(UIView*)parent
{
    for (UIView* view in [parent subviews]) {
        if([view isKindOfClass:[UILabel class]])
        {
            UILabel *label = (UILabel*)view;
            [label setTextAlignment:NSTextAlignmentCenter];
            [ViewHelper applyShadowToView:label WithSize:CGSizeMake(1, 1)];
            if(label.tag == 1245)//special tag,denotes time type label
            {
                [label setFont:FNT_CGB_F(32)];
            }
            else if(label.tag == 456)//title label
            {
                [label setFont:FNT_MUSEO_F(20)];
            }
            else if(label.tag == 1294)//code label
            {
                [label setFont:FNT_CGB_F(25)];
            }
            else
            {
                [label setFont:FNT_MUSEO_F(17)];
            }
        }
        else if([view isKindOfClass:[UITextView class]])
        {
            UITextView *tv = (UITextView *)view;
            [tv setTextAlignment:NSTextAlignmentCenter];
            [ViewHelper applyShadowToView:tv WithSize:CGSizeMake(1, 1)];
            [tv setFont:FNT_MUSEO_F(17)];
        }
        else
        {
            [self stylizeAllLabels:view];//recursively do same on all
        }
    }
}
-(void) callPerSecond:(NSTimer*) timer
{
    //always keep checking that if we are not showing the wrong view
    if([ViewHelper isTodayBeforeCampaign] && [tmLabels count] > 0)
    {
        int interval = [ViewHelper getGiveAwayStartsDaysLeft];
        int days = interval/86400;
        int left = interval%86400;
        int hours = left/3600;
        int moreLeft = left%3600;
        int mins = moreLeft/60;
        int secs = moreLeft%60;
        ((UILabel*)[tmLabels objectAtIndex:0]).text = [NSString stringWithFormat:@"%d",days];//days
        ((UILabel*)[tmLabels objectAtIndex:1]).text = [NSString stringWithFormat:@"%d",hours];
        ((UILabel*)[tmLabels objectAtIndex:2]).text = [NSString stringWithFormat:@"%d",mins];
        ((UILabel*)[tmLabels objectAtIndex:3]).text = [NSString stringWithFormat:@"%d",secs];
    }
    else if([ViewHelper isTodayAfterCampaign])
    {
        if(((UIView*)[self.contentPane.subviews objectAtIndex:0]).tag != AFTER_CAMPAIGN_VIEW)
        {
            [self doPrepareForAfterCampaign];
        }
    }
    else{
        //campaign going on
        int tag = ((UIView*)[self.contentPane.subviews objectAtIndex:0]).tag;
        if( tag != ON_CAMPAIGN_VIEW)
        {
            [self doPrepareForWithinCampaign];
        }
        else if(tag == ON_CAMPAIGN_VIEW)
        {
            NSString *gftID = [((UIView*)[self.contentPane.subviews objectAtIndex:0]) accessibilityIdentifier];
            GiftChoice *gft = [self getGiftWithID:gftID];
            if(gft != nil && ![gft isTodayValidForThisGift])
            {
                //showing a wrong gift,reload
                [self doPrepareForWithinCampaign];
            }
        }
    }
}
//decorate a view for contentpane if it's an after campaign day

- (UIView*)decorateViewForAfterCampaign
{    
    title = @"Thank you for participating in our holiday giveaway!";
    msg = @"Find your complimentary coupon code below to get 10% off on your ink order until the end of this month!";
    

    float margin = 10;
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.contentPane.frame.size.width, self.contentPane.frame.size.height)];
    
    
    float msgLabelHeight=120;
    UILabel *msgLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,0,view.frame.size.width,msgLabelHeight)];
    msgLabel.numberOfLines = 0;
    [msgLabel setTextAlignment:NSTextAlignmentCenter];
    msgLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [msgLabel setTextColor:[UIColor colorWithRed:252/255.0 green:247/255.0 blue:231/255.0 alpha:1]];
    [msgLabel setBackgroundColor:[UIColor clearColor]];
    msgLabel.text = msg;
    
    [view addSubview:msgLabel];
    
    UILabel *codeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, msgLabel.frame.size.height, view.frame.size.width,30)];
    [codeLabel setTextColor:[UIColor colorWithRed:237/255.0 green:24/255.0 blue:71/255.0 alpha:1]];
    codeLabel.tag = 1294;//tag for code label
    [codeLabel setBackgroundColor:[UIColor clearColor]];
    [codeLabel setTextAlignment:NSTextAlignmentCenter];
    [view addSubview:codeLabel];
    
    UILabel *chckWebLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, msgLabel.frame.size.height+margin+codeLabel.frame.size.height+margin, view.frame.size.width,30)];
    [chckWebLabel setTextColor:[UIColor colorWithRed:252/255.0 green:247/255.0 blue:231/255.0 alpha:1]];
    [chckWebLabel setBackgroundColor:[UIColor clearColor]];
    [chckWebLabel setTextAlignment:NSTextAlignmentCenter];
    [view addSubview:chckWebLabel];
    
    chckWebLabel.text = @"Check our website for more!";
    codeLabel.text = COMPLIMENTARY_CODE;
    
    [self.btnEditGfts removeFromSuperview];
    
    view.tag = AFTER_CAMPAIGN_VIEW;
    
    return view;
}

-(UIView*)decorateViewForBeforeCampaign
{
    float margin = 10;
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.contentPane.frame.size.width, self.contentPane.frame.size.height)];
    
    
    float msgLabelHeight = 120;
    
    CGSize maximumLabelSize = CGSizeMake(view.frame.size.width, FLT_MAX);
    CGSize expectedLabelSize = [msg sizeWithFont:FNT_MUSEO_F(17) constrainedToSize:maximumLabelSize lineBreakMode:nil];
    
    UILabel *msgLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,0,view.frame.size.width,expectedLabelSize.height)];
    [msgLabel setTextAlignment:NSTextAlignmentCenter];
    [msgLabel setTextColor:[UIColor colorWithRed:252/255.0 green:247/255.0 blue:231/255.0 alpha:1]];
    [msgLabel setBackgroundColor:[UIColor clearColor]];
    msgLabel.numberOfLines = 0;
    msgLabel.lineBreakMode = NSLineBreakByWordWrapping;
    msgLabel.text = msg;
    
    
    
    UIScrollView *msgLabelHolder = [[UIScrollView alloc] initWithFrame:CGRectMake(0,0,view.frame.size.width,msgLabelHeight)];
    [msgLabelHolder setBackgroundColor:[UIColor clearColor]];
    msgLabelHolder.showsHorizontalScrollIndicator = NO;
    msgLabelHolder.showsVerticalScrollIndicator = NO;
    msgLabelHolder.scrollEnabled = YES;
    [msgLabelHolder addSubview:msgLabel];
    msgLabelHolder.contentSize = expectedLabelSize;
    
    NSLog(@"Expected Height:%f,height given:%f",expectedLabelSize.height,msgLabelHeight);
    
    
    if(expectedLabelSize.height > msgLabelHeight)
    {
        float scrollPointsPerSecond =50;
        CGFloat maxOffsetWidth = msgLabelHolder.contentSize.width - CGRectGetWidth(msgLabelHolder.frame);
        
        CGFloat scrollingPointsPerSecond = 50.0;
        
        CGFloat time = maxOffsetWidth / scrollPointsPerSecond;
        
        [UIView animateWithDuration:time
                            delay:0.0
                            options:UIViewAnimationOptionCurveLinear|UIViewAnimationOptionAutoreverse | UIViewAnimationOptionRepeat
                         animations:^() {
                             msgLabelHolder.contentOffset = CGPointMake(maxOffsetWidth, 0);
                        }
                         completion:nil];
        
    }
    
    [view addSubview:msgLabelHolder];
    
    
    float tmLabelWidth = 50;
    float tmLabelHeight = 48.0;
    
    for(int i = 0; i < 4; i++)
    {
        UILabel *tmLabel = [[UILabel alloc] initWithFrame:CGRectMake(margin*(i+1)+tmLabelWidth*i,msgLabelHeight+margin,tmLabelWidth,tmLabelHeight)];
        tmLabel.tag = 1245;
        [tmLabel setTextAlignment:NSTextAlignmentCenter];
        [tmLabel setTextColor:[UIColor whiteColor]];
        [tmLabel setBackgroundColor:[UIColor clearColor]];
        [tmLabels addObject:tmLabel];
        
        //decoration
        UILabel *typeLabel = [[UILabel alloc] initWithFrame:CGRectMake(margin*(i+1)+tmLabelWidth*i,msgLabelHeight+tmLabelHeight,tmLabelWidth,tmLabelHeight)];
        [typeLabel setTextAlignment:NSTextAlignmentCenter];
        [typeLabel setTextColor:[UIColor redColor]];
        [typeLabel setBackgroundColor:[UIColor clearColor]];
        switch (i) {
            case 0:
                [typeLabel setText:@"days"];
                break;
            case 1:
                [typeLabel setText:@"hours"];
                break;
            case 2:
                [typeLabel setText:@"mins"];
                break;
            case 3:
                [typeLabel setText:@"secs"];
                break;
        }
        
        [view addSubview:tmLabel];
        [view addSubview:typeLabel];
    }
    
    UILabel *toGoLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,msgLabelHeight+2*tmLabelHeight,msgLabel.frame.size.width,30)];
    [toGoLabel setTextAlignment:NSTextAlignmentCenter];
    [toGoLabel setTextColor:[UIColor colorWithRed:252/255.0 green:247/255.0 blue:231/255.0 alpha:1]];
    [toGoLabel setBackgroundColor:[UIColor clearColor]];
    [toGoLabel setText:@"to go..."];
    [view addSubview:toGoLabel];
    
    view.tag = BEFORE_CAMPAIGN_VIEW;
    
    return view;
}

//decorate a view for contentpane if it's an oncampaign day
- (UIView *)decorateViewForOnCampaign
{   
    GiftChoice *selectedGift = [[GiftChoice alloc] init];
    NSLog(@"dVFOC:count %d",[[ViewHelper getSharedApplicationInstance].giftObjects count]);
    for(GiftChoice *choice in [ViewHelper getSharedApplicationInstance].giftObjects)
    {
        NSLog(@"%@",choice.date);
        if([choice isTodayValidForThisGift])
        {
            
            selectedGift.title = choice.title;
            selectedGift.date = choice.date;
            selectedGift.icon = choice.icon;
            selectedGift.ID =choice.ID;
            break;
        }
    }
    
    title = [NSString stringWithFormat:@"Order ink today to win %@",[selectedGift.title uppercaseString]];
    
    UIImageView *giftImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.contentPane.frame.size.width, self.contentPane.frame.size.height)];
    [giftImg setContentMode:UIViewContentModeScaleAspectFit];
    [giftImg setImage:selectedGift.icon];
    [giftImg setAccessibilityIdentifier:selectedGift.ID];
    
    
    giftImg.tag = ON_CAMPAIGN_VIEW;
    
    return giftImg;
}
//transitional animation for view entrance in contentPane
-(void)createTransitionEntranceInContentpane:(UIView*)newView
{
    UIView *prevView = nil;
    if([self.contentPane.subviews count] == 1)
    {
        prevView = [self.contentPane.subviews objectAtIndex:0];
    }
    [ViewHelper transitionFrom:prevView To:newView InRootView:self.contentPane];
}
//prepare the controller for before campaign schedule
- (void)doPrepareForBeforeCampaign
{
    UIView *newView=[self decorateViewForBeforeCampaign];
    [self.btnGoToPage setTitle:@"Go to campaign page" forState:UIControlStateNormal];
    urlToGo = [NSURL URLWithString:@"http://www.compandsave.com/christmas"];
    [self createTransitionEntranceInContentpane:newView];
    [self.lblTitle setText:title];
    //add shadows and specific fonts
    [self stylizeAllLabels:self.view];
}

- (void)doPrepareForWithinCampaign
{
    UIView *newView=[self decorateViewForOnCampaign];
    [self.btnGoToPage setTitle:@"Go to Giveaway page" forState:UIControlStateNormal];
    urlToGo = [NSURL URLWithString:@"http://www.compandsave.com/giveaway"];
    [self createTransitionEntranceInContentpane:newView];
    [self.lblTitle setText:title];
    //add shadows and specific fonts
    [self stylizeAllLabels:self.view];
}

-(void)doPrepareForAfterCampaign
{
    UIView *newView = [self decorateViewForAfterCampaign];
    [self.btnGoToPage setTitle:@"Go to COMPANDSAVE.COM" forState:UIControlStateNormal];
    urlToGo = [NSURL URLWithString:@"http://www.compandsave.com/"];
    [self createTransitionEntranceInContentpane:newView];
    [self.lblTitle setText:title];
    //add shadows and specific fonts
    [self stylizeAllLabels:self.view];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    tmLabels = [NSMutableArray array];

    
    title = [bundle objectForKey:@"title"];
    msg = [bundle objectForKey:@"msg"];
    
    if(title == nil || msg == nil)
    {
        if([[ViewHelper getSelectedChoiceList] count] > 0)
        {
            //exists previously selected choices
            title = @"Thank you for choosing your favorite gift item(s).";
            msg = @"You will be reminded to enter the giveaway when these items are available.\nCheck the campaign page for latest news and updates!";
        }
    
    }

    [ViewHelper applyShadowToView:self.btnEditGfts WithSize:CGSizeMake(1, 1)];
    
    [ViewHelper applyShadowToView:self.btnGoToPage WithSize:CGSizeMake(1, 1)];
    if([ViewHelper isTodayBeforeCampaign] )
    {
        [self doPrepareForBeforeCampaign];
    }
    
    if([ViewHelper isTodayWithinCampaign])
    {
        
        [self doPrepareForWithinCampaign];
        
        
    }
    if([ViewHelper isTodayAfterCampaign])
    {
        
        [self doPrepareForAfterCampaign];
    }
    
    
    //we need this timer not to update the times but also to keep views uptodate
    NSTimer* myTimer = [NSTimer scheduledTimerWithTimeInterval: 1.0 target: self
                                                      selector: @selector(callPerSecond:) userInfo: nil repeats: YES];
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
