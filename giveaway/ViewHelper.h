//
//  ViewHelper.h
//  giveaway
//
//  Created by Md. Abdul Munim Dibosh on 11/20/13.
//  Copyright (c) 2013 Md. Abdul Munim Dibosh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import "MainScreen.h"
#import "GiftChoice.h"
#import "Definitions.h"
#define SELECTED_GIFTS_KEY @"selectedItems"
#define COMPLIMENTARY_CODE @"XGH34J" //temp
#import "AppDelegate.h"

@interface ViewHelper : NSObject
+(void)showSplash:(UIView*)parent;
+(NSString*)md5HexDigest:(NSString*)input;
+(void)applyShadowToView:(UIView*)view WithSize:(CGSize)size;
+(void)showViewController:(NSString *)ID WithData:(NSDictionary*)dic;
+(BOOL)isTodayBeforeCampaign;
+(BOOL)isTodayWithinCampaign;
+(BOOL)isTodayAfterCampaign;
+(NSTimeInterval)getGiveAwayStartsDaysLeft;
+(NSTimeInterval)getGiveAwayEndsDaysLeft;
// can return:
//1.the list of objects which can selected as choice by user,should not show the choice of a previous day
//2.all the gift objects
//choice=YES,returns valid choice objects
+(NSMutableArray*)getGiftObjectsArray:(BOOL)choice;
+(void) scheduleNotificationForDate:(NSDate *)date AlertBody:(NSString *)alertBody ActionButtonTitle:(NSString *)actionButtonTitle NotificationID:(NSString *)notificationID WithSeverity:(BOOL)severe;
+(void)cancelLocalNotification:(NSString*)notificationID;
+(void)saveSelectedChoiceList:(NSArray*)array;
+(NSArray*)getSelectedChoiceList;

+(AppDelegate*)getSharedApplicationInstance;

+(void)transitionFrom:(UIView*)view1 To:(UIView*)view2 InRootView:(UIView*)parent;

@end
