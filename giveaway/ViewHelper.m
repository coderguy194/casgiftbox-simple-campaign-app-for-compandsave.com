//
//  ViewHelper.m
//  giveaway
//
//  Created by Md. Abdul Munim Dibosh on 11/20/13.
//  Copyright (c) 2013 Md. Abdul Munim Dibosh. All rights reserved.
//

#import "ViewHelper.h"
#import <CommonCrypto/CommonDigest.h>
#import "AppDelegate.h"

@implementation ViewHelper
static UIImageView *launchLowerImage;
static UIImageView *launchUpperImage;
static UIView *splashView;
//IF WE NEED THIS EVER
//-(void)goUp
//{
//    //    [UIView beginAnimations:@"ViewUp" context:nil];
//    //    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
//    //    [UIView setAnimationDuration:0.3];
//    //    [UIView setAnimationDelegate:self];
//    //    [UIView setAnimationDidStopSelector:@selector(reachedUp:)];
//    //
//    //    // Make the animatable changes.
//    //    self.bottomOverlay.center = locationTop;
//    //
//    //    // Commit the changes and perform the animation.
//    //    [UIView commitAnimations];
//    
//    [UIView animateWithDuration:0.2
//                          delay:0.0
//                        options:(UIViewAnimationCurveEaseInOut|UIViewAnimationOptionAllowUserInteraction)
//                     animations:^{
//                         
//                         self.bottomOverlay.center = locationTop;
//                         
//                     }
//                     completion:^(BOOL finished){
//                         canAnimate = YES;
//                     }];
//}
//
//-(void)goDown
//{
//    //    [UIView beginAnimations:@"ViewDown" context:nil];
//    //    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
//    //    [UIView setAnimationDuration:0.3];
//    //    [UIView setAnimationDelegate:self];
//    //    [UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:)];
//    //
//    //    // Make the animatable changes.
//    //    self.bottomOverlay.center = locationBottom;
//    //
//    //    // Commit the changes and perform the animation.
//    //    [UIView commitAnimations];
//    
//    [UIView animateWithDuration:0.2
//                          delay:0.0
//                        options:(UIViewAnimationCurveEaseInOut|UIViewAnimationOptionAllowUserInteraction)
//                     animations:^{
//                         
//                         self.bottomOverlay.center = locationBottom;
//                         
//                     }
//                     completion:^(BOOL finished){
//                         canAnimate = YES;
//                     }];
//}

+ (void)transitionToViewController:(UIViewController *)viewController InApp:(AppDelegate*)delegate
                    withTransition:(UIViewAnimationOptions)transition
{
    [UIView transitionFromView:delegate.window.rootViewController.view
                        toView:viewController.view
                      duration:0.65f
                       options:transition
                    completion:^(BOOL finished){
                        ((AppDelegate*)[UIApplication sharedApplication].delegate).window.rootViewController = viewController;
                    }];
}
+(void)vanishDisclaimer
{
    [UIView animateWithDuration:0.5
                          delay:4.0
                        options:(UIViewAnimationCurveEaseInOut|UIViewAnimationOptionAllowUserInteraction)
                     animations:^{
                         
                         splashView.alpha = 0;                         
                     }
                     completion:^(BOOL finished){
                         //recycle view1
                         [splashView removeFromSuperview];
                     }];
}
//vanishes the launcher images with animation,private method
+(void)vanishSplash
{
    [UIView animateWithDuration:0.3
                          delay:2.0
                        options:(UIViewAnimationCurveEaseInOut|UIViewAnimationOptionAllowUserInteraction)
                     animations:^{
                         
                         launchUpperImage.center = CGPointMake(-launchUpperImage.center.x, launchUpperImage.center.y);
                         launchLowerImage.center = CGPointMake(launchLowerImage.center.x+320, launchLowerImage.center.y);
                         
                         
                     }
                     completion:^(BOOL finished){
                         
                         [launchLowerImage removeFromSuperview];
                         [launchUpperImage removeFromSuperview];
                         [self vanishDisclaimer];
                     }];
    
    
}


+(void)showSplash:(UIView*)parent
{
    float upperPartRatio = 0.2;
    float parentHeight = parent.frame.size.height;
    float parentWidth = parent.frame.size.width;
    float upperPartHeight = upperPartRatio*parentHeight;
    
    launchUpperImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, parentWidth,upperPartHeight)];
    launchUpperImage.contentMode = UIViewContentModeScaleToFill;
    launchLowerImage = [[UIImageView alloc] initWithFrame:CGRectMake(0,upperPartHeight, parentWidth, parentHeight - upperPartHeight)];
    launchLowerImage.contentMode = UIViewContentModeScaleToFill;
    
    launchUpperImage.image = [UIImage imageNamed:@"launch_top2.png"];
    launchLowerImage.image = [UIImage imageNamed:@"launch_bottom.png"];
    
    
    
    splashView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, parentWidth,parentHeight)];
    [splashView setBackgroundColor:[UIColor colorWithRed:252/255.0 green:247/255.0 blue:231/255.0 alpha:1]];
    
    //disclaimer view
    float iconWidth = 200.0;
    float iconHeight = 120.0;
    float labelHeight = 250.0;
    float margin = 20;
    
    UIImageView *casLogo = [[UIImageView alloc] initWithFrame:CGRectMake(parentWidth/2-iconWidth/2,parentHeight/4-iconHeight/2,iconWidth, iconHeight)];
    [casLogo setContentMode:UIViewContentModeScaleAspectFit];
    [casLogo setImage:[UIImage imageNamed:@"cas_logo.png"]];
    //shadow
//    [self applyShadowToView:appleIcon WithSize:CGSizeMake(1, 1)];
    
    [splashView addSubview:casLogo];
    
    NSString *msg = @"“This App is designed to track CompAndSave.com’s Giveaway item calendar and it can also be used to get future discounts and special offers.\nApple or any other brand is not a participant in or sponsor of this promotion. This Campaign is solely sponsored by CompAndSave.com Inc.”";
    CGSize maximumLabelSize = CGSizeMake(parentWidth-2*margin, FLT_MAX);
    CGSize expectedLabelSize = [msg sizeWithFont:FNT_MUSEO_F(17) constrainedToSize:maximumLabelSize lineBreakMode:nil];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(parentWidth/2-(parentWidth-2*margin)/2, casLogo.frame.origin.y+casLogo.frame.size.height+ margin, parentWidth-2*margin,expectedLabelSize.height)];
    [label setText:msg];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setNumberOfLines:0];
    [label setFont:FNT_MUSEO_F(17)];
    [label setTextColor:[UIColor colorWithRed:237/255.0 green:24/255.0 blue:71/255.0 alpha:1]];
    [label setBackgroundColor:[UIColor clearColor]];
    
//    [self applyShadowToView:label WithSize:CGSizeMake(0.5, 0.5)];
    
    [splashView addSubview:label];
    
    [splashView addSubview:launchLowerImage];
    [splashView addSubview:launchUpperImage];
    
    //as safety
    [splashView bringSubviewToFront:launchLowerImage];
    [splashView bringSubviewToFront:launchUpperImage];
    
    [parent addSubview:splashView];
    
    //as safety
    [parent bringSubviewToFront:splashView];
    
    [self vanishSplash];
    
}
+(NSString*)md5HexDigest:(NSString*)input{
    const char* str = [input UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(str, strlen(str), result);
    
    NSMutableString *ret = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH*2];
    for(int i = 0; i<CC_MD5_DIGEST_LENGTH; i++) {
        [ret appendFormat:@"%02x",result[i]];
    }
    return ret;
}

//SOME BEAUTIFYING CODE
+(void)applyShadowToView:(UIView*)view WithSize:(CGSize)size
{
    view.layer.shadowOpacity = 1.0;
    view.layer.shadowRadius = 0.0;
    view.layer.shadowColor = [UIColor blackColor].CGColor;
    view.layer.shadowOffset = size;
}

+(void)showViewController:(NSString *)ID WithData:(NSDictionary*)dic
{
    AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    UIViewController *mainScreen = (UIViewController*)[delegate.storyboard instantiateViewControllerWithIdentifier:ID];
    if(dic != nil)
    {
        ((MainScreen*)mainScreen).bundle = dic;
    }
    [self transitionToViewController:mainScreen InApp:delegate withTransition:UIViewAnimationOptionTransitionFlipFromRight];
}

+(BOOL)isTodayBeforeCampaign
{
    NSDate *today = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm a"];
    NSDate *day = [dateFormatter dateFromString:@"2013-12-16 12:00 AM"];
    NSComparisonResult compare = [day compare:today];
    
    return (compare == NSOrderedDescending);
}

+(BOOL)isTodayWithinCampaign
{
    NSDate *today = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm a"];
    NSDate *firstDay = [dateFormatter dateFromString:@"2013-12-16 12:00 AM"];
    NSDate *lastDay = [dateFormatter dateFromString:@"2014-01-01 12:00 AM"];
    NSComparisonResult compare1 = [firstDay compare:today];
    NSComparisonResult compare2 = [lastDay compare:today];
    
    NSLog(@"Called this");
    
    return (compare1 == NSOrderedAscending && compare2 == NSOrderedDescending);
}

+(BOOL)isTodayAfterCampaign
{
    NSDate *today = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm a"];
    NSDate *day = [dateFormatter dateFromString:@"2014-01-01 12:00 AM"];
    NSComparisonResult compare = [day compare:today];
    
    return (compare == NSOrderedSame || compare == NSOrderedAscending);
}

+(NSTimeInterval)getGiveAwayStartsDaysLeft
{
    NSDate *today = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm a"];
    NSDate *endday = [dateFormatter dateFromString:@"2013-12-16 12:00 AM"];
    NSTimeInterval interval = [endday timeIntervalSinceDate:today];
    return interval;
}
+(NSTimeInterval)getGiveAwayEndsDaysLeft
{
    NSDate *today = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm a"];
    NSDate *endday = [dateFormatter dateFromString:@"2014-01-01 12:00 AM"];
    NSTimeInterval interval = [endday timeIntervalSinceDate:today];
    return interval;
}
+ (GiftChoice *)getGiftChoice:(NSArray*)array
{
    GiftChoice *choice = [[GiftChoice alloc] init];
    [choice setIsSelected:NO];
    [choice setTitle:[array objectAtIndex:0]];
    [choice setDate:[array objectAtIndex:1]];
    [choice setIcon:[UIImage imageNamed:[array objectAtIndex:2]]];
    [choice setAlarmDay:[[array lastObject] intValue]];
    NSLog(@"ViewHelper::date:%@",[array lastObject]);
    //MD5 unique ID
    [choice setID:[ViewHelper md5HexDigest:choice.title]];
    return choice;
}
+(NSArray*)arrayOfGiftObjectStrings
{
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"GiftChoices" ofType:@"plist"];
    NSDictionary *giftChoicesDic = [[NSDictionary alloc] initWithContentsOfFile:plistPath];
    NSArray *giftChoices = [NSArray arrayWithArray:[giftChoicesDic objectForKey: @"choices"]];
    return giftChoices;
}
+(NSMutableArray*)getGiftObjectsArray:(BOOL)choice
{
    NSMutableArray *giftChoiceObjects = [NSMutableArray array];
    
    NSArray *giftChoices = [self arrayOfGiftObjectStrings];
    
    NSDate *today = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm a"];
    
    for (NSString *str in giftChoices) {
        
        NSArray *firstSplit = [str componentsSeparatedByString:@"@"];
        NSString *date = [firstSplit lastObject];
        NSDate *day = [dateFormatter dateFromString:[NSString stringWithFormat:@"2013-12-%@ 12:00 AM",date]];
        NSComparisonResult compare = [day compare:today];
        //means day is later than today in time
        //we should not show him the choices which are before today
        if(choice)
        {
            if(compare == NSOrderedDescending)
            {
                [giftChoiceObjects addObject:[self getGiftChoice:firstSplit]];
                
            }
        }
        else{
            [giftChoiceObjects addObject:[self getGiftChoice:firstSplit]];
        }
        
    }
    NSLog(@"ViewHelper:%d",[giftChoiceObjects count]);
    return giftChoiceObjects;
}
+(void) scheduleNotificationForDate:(NSDate *)date AlertBody:(NSString *)alertBody ActionButtonTitle:(NSString *)actionButtonTitle NotificationID:(NSString *)notificationID WithSeverity:(BOOL)severe{
    
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    // Break the date up into components
    NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit )
												   fromDate:date];
    NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit )
												   fromDate:date];
    // Set up the fire time
    NSDateComponents *dateComps = [[NSDateComponents alloc] init];
    [dateComps setDay:[dateComponents day]];
    [dateComps setMonth:[dateComponents month]];
    [dateComps setYear:[dateComponents year]];
    [dateComps setHour:[timeComponents hour]];
	// Notification will fire in one minute
    [dateComps setMinute:[timeComponents minute]];
    
    NSLog(@"day:%d hours:%d min:%d",[dateComponents day],[timeComponents hour],[timeComponents minute]);
    
    NSDate *itemDate = [calendar dateFromComponents:dateComps];
    
    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    localNotification.fireDate = itemDate;
    localNotification.timeZone = [NSTimeZone defaultTimeZone];
    localNotification.alertBody = alertBody;
    localNotification.alertAction = actionButtonTitle;
    if(severe)
    {
        localNotification.soundName = @"Bloom.wav";
    }else{
        localNotification.soundName = @"Turn.wav";
    }
    int days = [self getGiveAwayEndsDaysLeft]/86400;
    localNotification.applicationIconBadgeNumber = days;//days left to end the giveaway
    NSLog(@"days to end:%d",days);
    NSDictionary *infoDict = [NSDictionary dictionaryWithObject:notificationID forKey:notificationID];
    localNotification.userInfo = infoDict;
    
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}
+(void)cancelLocalNotification:(NSString*)notificationID {
    //loop through all scheduled notifications and cancel the one we're looking for
    UILocalNotification *cancelThisNotification = nil;
    BOOL hasNotification = NO;
    
    for (UILocalNotification *someNotification in [[UIApplication sharedApplication] scheduledLocalNotifications]) {
        if([[someNotification.userInfo objectForKey:notificationID] isEqualToString:notificationID]) {
            cancelThisNotification = someNotification;
            hasNotification = YES;
            break;
        }
    }
    if (hasNotification == YES) {
        NSLog(@"%@ ",cancelThisNotification);
        [[UIApplication sharedApplication] cancelLocalNotification:cancelThisNotification];
    }
}

+(void)saveSelectedChoiceList:(NSArray*)array
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:array forKey:SELECTED_GIFTS_KEY];
    [defaults synchronize];
}
+(NSArray*)getSelectedChoiceList
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults arrayForKey:SELECTED_GIFTS_KEY];
}

+(AppDelegate*)getSharedApplicationInstance{
    return (AppDelegate*)[UIApplication sharedApplication].delegate;
}

+(void)transitionFrom:(UIView*)view1 To:(UIView*)view2 InRootView:(UIView*)parent
{
    int margin = 5;//for safety and smoothing
    if(view1 != nil)
    {
        //where the view2 will be at first
        CGPoint view2OutPoint = CGPointMake(2*parent.frame.size.width, parent.frame.size.height/2);
        //where it will be after animation
        CGPoint view2InPoint = CGPointMake(parent.frame.size.width/2, parent.frame.size.height/2);
        view2.alpha = 0;
        view2.center = view2OutPoint;
        //where the view1 will be after animation
        CGPoint view1OutPoint = CGPointMake(-2*parent.frame.size.width, parent.frame.size.height/2);
        //add view2 to main view
        [parent addSubview:view2];
        
        [UIView animateWithDuration:0.5
                              delay:0.0
                            options:(UIViewAnimationCurveEaseInOut|UIViewAnimationOptionAllowUserInteraction)
                         animations:^{
                             //as view1 can be nil if the parent didnt have any view before
                             view1.center = view1OutPoint;
                             view1.alpha = 0;
                             view2.center = view2InPoint;
                             view2.alpha = 1;
                             
                         }
                         completion:^(BOOL finished){
                             //recycle view1
                             [view1 removeFromSuperview];
                         }];
    }
    else{
        [parent addSubview:view2];
    }
}

@end
