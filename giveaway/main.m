//
//  main.m
//  giveaway
//
//  Created by Md. Abdul Munim Dibosh on 11/16/13.
//  Copyright (c) 2013 Md. Abdul Munim Dibosh. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
